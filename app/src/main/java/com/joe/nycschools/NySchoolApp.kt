package com.joe.nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NySchoolApp : Application() {
}