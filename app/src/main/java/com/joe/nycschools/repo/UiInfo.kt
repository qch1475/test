package com.joe.nycschools.repo

import com.joe.nycschools.repo.Status.ERROR
import com.joe.nycschools.repo.Status.LOADING
import com.joe.nycschools.repo.Status.SUCCESS
import java.lang.Exception

data class UiInfo<out T>(val status: Status, val data: T?, val message: String?, val exception: Exception?) {

    companion object {
        @JvmStatic
        fun <T> success(data: T? = null): UiInfo<T> {
            return UiInfo(SUCCESS, data, null, null)
        }

        @JvmOverloads
        @JvmStatic
        fun <T> error(msg: String? = null, exception: Exception? = null, data: T? = null): UiInfo<T> {
            return UiInfo(ERROR, data, msg, exception)
        }

        @JvmOverloads
        @JvmStatic
        fun <T> loading(data: T? = null): UiInfo<T> {
            return UiInfo(LOADING, data, null, null)
        }
    }
}
