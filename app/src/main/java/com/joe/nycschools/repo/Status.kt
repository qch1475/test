package com.joe.nycschools.repo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
