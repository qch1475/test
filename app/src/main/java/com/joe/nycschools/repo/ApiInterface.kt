package com.joe.nycschools.repo

import com.joe.nycschools.model.HighSchoolDTO
import com.joe.nycschools.model.SATScoresDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getSchoolList(): Response<List<HighSchoolDTO>>

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSatScores(@Query("dbn") dbn: String): Response<List<SATScoresDTO>>
}

