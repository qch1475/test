package com.joe.nycschools.repo

object ApiConstants {

    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    const val SCHOOL_LIST_ENDPOINT = "s3k6-pzi2.json"
    const val SAT_SCORE_ENDPOINT = "f9bf-2cp4.json"
}