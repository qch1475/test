package com.joe.nycschools.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.joe.nycschools.databinding.SchoolDetailsBinding
import com.joe.nycschools.model.HighSchoolDTO
import com.joe.nycschools.model.SATScoresDTO
import com.joe.nycschools.repo.Status.ERROR
import com.joe.nycschools.repo.Status.LOADING
import com.joe.nycschools.repo.Status.SUCCESS
import com.joe.nycschools.repo.UiInfo
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {
    private var binding: SchoolDetailsBinding? = null
    val homeViewModel: HomeViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, bundle: Bundle?
    ): View? {
        binding = SchoolDetailsBinding.inflate(inflater, container, false)
        val root: View = binding!!.root
        var highSchoolDTO: HighSchoolDTO? = null
        if (arguments != null) {
            highSchoolDTO = requireArguments().getSerializable(SCHOOL_DETAILS) as HighSchoolDTO?
        }
        if (highSchoolDTO != null) {
            homeViewModel.getSatScores(highSchoolDTO.dbn)
                .observe(viewLifecycleOwner) { listUiInfo: UiInfo<SATScoresDTO?> ->
                    loadSATScores(listUiInfo)
                }
        }
        return root
    }

    private fun loadSATScores(listUiInfo: UiInfo<SATScoresDTO?>) {
        when (listUiInfo.status) {
            SUCCESS -> {
                // Data fetch was successful. Updating SAT scores in school details screen
                val satScoresDTO = listUiInfo.data
                if (satScoresDTO != null) {
                    binding!!.schoolName.text = satScoresDTO.school_name
                    binding!!.numOfSatTestTakersValue.text = satScoresDTO.num_of_sat_test_takers
                    binding!!.satMathAvgScore.text = satScoresDTO.sat_math_avg_score
                    binding!!.satCriticalReadingAvgScore.text =
                        satScoresDTO.sat_critical_reading_avg_score
                    binding!!.satWritingAvgScore.text = satScoresDTO.sat_writing_avg_score
//                    binding!!.satError.visibility = View.GONE
//                    binding!!.satTotal.visibility = View.VISIBLE
//                    binding!!.satReading.visibility = View.VISIBLE
//                    binding!!.satMath.visibility = View.VISIBLE
//                    binding!!.satWriting.visibility = View.VISIBLE
                } else {
//                    binding!!.satError.setText(string.no_sat_score)
//                    binding!!.satError.visibility = View.VISIBLE
                }
            }

            LOADING -> {
//                binding!!.satError.setText(string.data_loading)
//                binding!!.satError.visibility = View.VISIBLE
            }

            ERROR -> {
                Log.d(TAG, listUiInfo.message!!)
//                binding!!.satError.text = listUiInfo.status.toString()
//                binding!!.satError.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        const val SCHOOL_DETAILS = "school_details"
        private const val TAG = "SchoolDetailsFragment"
    }
}