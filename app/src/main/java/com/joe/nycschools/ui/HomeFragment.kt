package com.joe.nycschools.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.joe.nycschools.R
//import com.joe.nycschools.app.NySchoolApp
import com.joe.nycschools.databinding.FragmentHomeBinding
import com.joe.nycschools.model.HighSchoolDTO
import com.joe.nycschools.repo.Status.ERROR
import com.joe.nycschools.repo.Status.LOADING
import com.joe.nycschools.repo.Status.SUCCESS
import com.joe.nycschools.repo.UiInfo
import com.joe.nycschools.ui.SchoolListAdapter.ListItemClickListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), ListItemClickListener {
    private var binding: FragmentHomeBinding? = null
    val homeViewModel: HomeViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding!!.root
        homeViewModel.getSchoolList()
            .observe(viewLifecycleOwner) { listUiInfo: UiInfo<List<HighSchoolDTO?>?> ->
                loadData(listUiInfo)
            }
        return root
    }

    private fun loadData(listUiInfo: UiInfo<List<HighSchoolDTO?>?>) {
        when (listUiInfo.status) {
            SUCCESS -> {
                // if successful. Updating display with list of schools
                binding!!.textHome.visibility = View.GONE
                val highSchoolDTOs = listUiInfo.data
                val size = highSchoolDTOs?.size ?: 0
                if (size != 0) {
                    displaySchoolList(highSchoolDTOs)
                }
            }

            LOADING ->
                binding!!.textHome.text = getString(R.string.data_loading)

            ERROR -> {
                // Display the error message to user.
                Log.d(TAG, listUiInfo.message!!)
                binding!!.textHome.text = listUiInfo.message
            }
        }
    }

    private fun displaySchoolList(highSchoolDTOS: List<HighSchoolDTO?>?) {
        val recyclerView = binding!!.schoolListRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = SchoolListAdapter(highSchoolDTOS, this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onListItemClick(position: Int, highSchoolDTO: HighSchoolDTO?) {
        val parent = parentFragment
        if (highSchoolDTO != null && parent != null) {
            val navController = findNavController(requireActivity(), parent.id)
            val bundle = Bundle()
            bundle.putSerializable(SchoolDetailsFragment.SCHOOL_DETAILS, highSchoolDTO)
            navController.navigate(R.id.navigation_school_details, bundle)
        }
    }

    companion object {
        private const val TAG = "HomeFragment"
    }
}