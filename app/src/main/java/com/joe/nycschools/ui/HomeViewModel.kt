package com.joe.nycschools.ui;

import android.util.Log
import androidx.lifecycle.*
import com.joe.nycschools.model.HighSchoolDTO;
import com.joe.nycschools.model.SATScoresDTO
import com.joe.nycschools.repo.ApiInterface
import com.joe.nycschools.repo.UiInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
      val repo: ApiInterface
) : ViewModel() {
    private val schoolList:
            MutableLiveData<UiInfo<List<HighSchoolDTO?>?>> = MutableLiveData()
    private val satScores: MutableLiveData<UiInfo<SATScoresDTO?>> = MutableLiveData()

     fun getSchoolList(): MutableLiveData<UiInfo<List<HighSchoolDTO?>?>> {
        schoolList.postValue(UiInfo.loading(null))
        viewModelScope.launch {
            try {
                val response = repo.getSchoolList()
                if (response.isSuccessful && response.body() != null)
                    schoolList.postValue(UiInfo.success(response.body()))
                else
                    schoolList.postValue(
                        UiInfo.error(response.message(),
                        null, null))
            } catch (e: java.lang.Exception) {
                Log.e(TAG, e.toString())
                schoolList.postValue(
                    UiInfo.error(
                        "Error Occurred while fetching School list: $e",
                        e,
                        null
                    )
                )
            }
        }
        return schoolList

    }

     fun getSatScores(dbn: String): MutableLiveData<UiInfo<SATScoresDTO?>> {
        satScores.postValue(UiInfo.loading(null))
        viewModelScope.launch {
            try {
                val response = repo.getSatScores(dbn)
                Log.d("shrevs", response.body().toString())
                val satScoresDTO = if(response.body() != null && response.body()!!.isNotEmpty()) {
                    response.body()?.get(0)
                } else {
                    null
                }
                if (response.isSuccessful && satScoresDTO!=null)
                    satScores.postValue(UiInfo.success(satScoresDTO))
                else
                    satScores.postValue(
                        UiInfo.error(response.message(), null, null)
                    )
            } catch (e: java.lang.Exception) {
                Log.e(TAG, e.toString())
                satScores.postValue(
                    UiInfo.error(
                        "Error Occurred while fetching SAT Scores: $e",
                        e,
                        null
                    )
                )
            }
        }
        return satScores

    }

    companion object {
        const val TAG = "HomeViewModel"
    }

}

